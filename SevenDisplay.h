#ifndef SEVENDISPLAY_H
#define SEVENDISPLAY_H
#include "Arduino.h"

class PinDisplay {
public:
	virtual void pinMode(int mode);
	virtual void digitalWrite(int state);
	virtual int digitalRead();
	virtual void digitalRefresh();

	void setType(int type) {
		if (type != HIGH && type != LOW) 
			return;
		this->type = type;
	}

	int getType() {
		return type;
	}
protected:
	int type = HIGH;
};

class ArrayPinDisplay : public PinDisplay {
public:
	ArrayPinDisplay(int amount) {
		this->pinArray = new PinDisplay*[amount];
		this->amount = amount;
	}

	virtual void pinMode(int mode) {
		for (int i = 0; i < amount; i++) {
			if (pinArray[i] != NULL)
				pinArray[i]->pinMode(!!(mode & (1 << i)));
		}
	}

	virtual void digitalWrite(int state) {
		//Serial.print("Ustawienie tablicy pinow na ");
		//Serial.println(state, BIN);
		for (int i = 0; i < amount; i++) {
			if (pinArray[i] != NULL)
				pinArray[i]->digitalWrite(!!(state & (1 << i)));
		}
	}

	virtual int digitalRead() {
		int state = 0;
		for (int i = 0; i < amount; i++) {
			if (pinArray[i] != NULL)
				state = !!pinArray[i]->digitalRead() << i;
		}
		return -1;
	}

	virtual void digitalRefresh() {
		for (int i = 0; i < amount; i++) {
			if (pinArray[i] != NULL)
				pinArray[i]->digitalRefresh();
		}
	}

	void addPin(PinDisplay* pin, int queue) {
		pinArray[queue] = pin;
	}

	void setType(int type) {
		this->type = type;
		for (int i = 0; i < amount; i++) {
			if (pinArray[i] != NULL)
				pinArray[i]->setType(type);
		}
	}
private: 
	PinDisplay **pinArray;
	int amount;
};


class PinArduino : public PinDisplay {
public:
	PinArduino(int pin):pin(pin) {}

	virtual void pinMode(int mode) {
		::pinMode(this->pin, mode);
	}

	virtual void digitalWrite(int state) {
		if (type == LOW)
			state = !state;
		::digitalWrite(this->pin, state);
	}

	virtual int digitalRead() {
		int state = ::digitalRead(this->pin);
		if (type == LOW)
			state = !state;
		return state;
	}

	virtual void digitalRefresh() {
	}
private:
	int pin;
};

#include "ShiftRegister.h"
//#ifdef SHIFTREGISTER_H
class PinShiftRegister : public PinDisplay {
public:
	PinShiftRegister(ShiftRegister* reg, int pin):pin(pin) {
		this->reg = reg;
	}

	virtual void pinMode(int mode) {
	}

	virtual void digitalWrite(int state) {
		if (reg->getType() != type)
			state = !state;
		reg->pinWrite(this->pin, state);
	}

	virtual int digitalRead() {
		return -1;
	}

	virtual void digitalRefresh() {
		reg->pinRefresh();
	}
private:
	ShiftRegister* reg;
	int pin;
};
//#endif

#ifdef PCF8574_H

class PinExpander : public PinDisplay {
public:
	PinShiftRegister(PCF8574* expand, int pin):pin(pin) {
		this->expand = expand;
	}

	virtual void pinMode(int mode) {
		expand->pinMode(pin, mode);
	}

	virtual void digitalWrite(int state) {
		if (type == LOW)
			state = !state;
		expand->digitalWrite(pin, state);
	}

	virtual int digitalRead() {
		int state = expand->digitalRead(pin);
		if (type == LOW)
			state = !state;
		return state;
	}

	virtual void digitalRefresh() {
	}
private:
	PCF8574* expand;
	int pin;
};
#endif


class SevenDisplay {
public: 
	SevenDisplay(int);

	void addSegment(int, int);
	void addDigit(int, int);

	#ifdef PCF8574_H
	void addSegmentExpander(int, int, PCF8574*);
	void addDigitExpander(int, int, PCF8574*);
	#endif

	//#ifdef SHIFTREGISTER_H
	void addShift() {}
	void addSegmentShift(int, int, ShiftRegister*);
	void addDigitShift(int, int, ShiftRegister*);
	//#endif

	void display();
	void setNumber(int);
	int getNumber();
	void setType(int);
	int getType();
	void setZerofill(bool);
	bool isZerofill();
	void setPoint(int);
	int getPoint();
	void setMinusEnable(bool);
	bool isMinusEnable();
private:
	//PinDisplay **digits, **segments;
	ArrayPinDisplay *digits, *segments;
	bool zerofill = false, minus = true;
	int type = HIGH, digitAmount, value = 0, dpIndex = -1;
	int *buffor;
	const int digitScheme[10] = {
	  0b11111100,
	  0b01100000,
	  0b11011010,
	  0b11110010,
	  0b01100110,
	  0b10110110,
	  0b10111110,
	  0b11100000,
	  0b11111110,
	  0b11110110
	};
};

#endif