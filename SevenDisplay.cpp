#include "SevenDisplay.h"

SevenDisplay::SevenDisplay(int digitAmount) {
	this->digitAmount = digitAmount;
	this->buffor = new int[digitAmount];
	this->digits = new ArrayPinDisplay(digitAmount);
	this->segments = new ArrayPinDisplay(8);
}

void SevenDisplay::addSegment(int pin, int segment) {
	PinDisplay *pinObj = new PinArduino(pin);
	pinObj->pinMode(OUTPUT);
	pinObj->setType(type);
	segments->addPin(pinObj, segment);
}

void SevenDisplay::addDigit(int pin, int digit) {
	PinDisplay *pinObj = new PinArduino(pin);
	pinObj->pinMode(OUTPUT);
	pinObj->setType(!type);
	digits->addPin(pinObj, digit);
}

#ifdef PCF8574_H
void SevenDisplay::addSegmentExpander(int pin, int segment, PCF8574* expander) {
	PinDisplay *pinObj = new PinExpander(expander, pin);
	pinObj->pinMode(OUTPUT);
	pinObj->setType(type);
	segments->addPin(pinObj, segment);
	
}

void SevenDisplay::addDigitExpander(int pin, int digit, PCF8574* expander) {
	PinDisplay *pinObj = new PinExpander(expander, pin);
	pinObj->pinMode(OUTPUT);
	pinObj->setType(!type);
	digits->addPin(pinObj, digit);
}
#endif

//#ifdef SHIFTREGISTER_H
void SevenDisplay::addSegmentShift(int pin, int segment, ShiftRegister* reg) {
	PinDisplay *pinObj = new PinShiftRegister(reg, pin);
	pinObj->pinMode(OUTPUT);
	pinObj->setType(type);
	segments->addPin(pinObj, segment);
}

void SevenDisplay::addDigitShift(int pin, int digit, ShiftRegister* reg) {
	PinDisplay *pinObj = new PinShiftRegister(reg, pin);
	pinObj->pinMode(OUTPUT);
	pinObj->setType(!type);
	digits->addPin(pinObj, digit);
}
//#endif

void SevenDisplay::display() {
	for (int i = 0; i < digitAmount; i++) {
		//Serial.println("Realizacja pinu cyfrowego");
		digits->digitalWrite(1 << i);
		digits->digitalRefresh();
		//Serial.println("Realizacja pinu rejestru");
		segments->digitalWrite(buffor[i]);
		segments->digitalRefresh();
		delay(1);
		//Serial.println("Resetowanie pinu rejestru");
		segments->digitalWrite(0);
		segments->digitalRefresh();
		//Serial.println("Resetowanie pinu cyfrowego");
		digits->digitalWrite(0);
		digits->digitalRefresh();
	}
}

void SevenDisplay::setNumber(int value) {
	this->value = value;

	int numberTmp = value < 0 ? -value : value;
	for (int i = 0; i < digitAmount; i++) {
		int nd = -1;
		int bitValue = 0;

		if (numberTmp > 0) {
			nd = numberTmp % 10;
			numberTmp /= 10;
			bitValue = digitScheme[nd];
		} else {
			if (zerofill) {
				bitValue = digitScheme[0];
			}
		}

		if (i == digitAmount-1 && minus && value < 0)
			bitValue = 0b10;

		if (dpIndex == i) {
			if (nd < 0)
				nd = 0;
			bitValue |= 1;
		}
		buffor[i] = bitValue;
		//Serial.print("Stanowisko ");
		//Serial.print(i);
		//Serial.print(" ma wartosc ");
		//Serial.println(bitValue, BIN);
	}
}

int SevenDisplay::getNumber() {
	return value;
}

void SevenDisplay::setType(int type) {
	if (type != HIGH && type != LOW) 
		return;
	segments->setType(type);
	digits->setType(!type);
	this->type = type;
}

int SevenDisplay::getType() {
	return type;
}

void SevenDisplay::setZerofill(bool zerofill) {
	this->zerofill = zerofill;
}

bool SevenDisplay::isZerofill() {
	return zerofill;
}

void SevenDisplay::setPoint(int dpIndex) {
	this->dpIndex = dpIndex;
}

int SevenDisplay::getPoint() {
	return dpIndex;
}

void SevenDisplay::setMinusEnable(bool minus) {
	this->minus = minus;
}

bool SevenDisplay::isMinusEnable() {
	return minus;
}